import React, { createContext, useContext, useReducer } from 'react'
import ReactDom from 'react-dom'
import { Todo } from './components/Todo/Todo.jsx'
import { Head } from './components/Head/Head.jsx'
import { reducers } from './reducers/index.js'

const ReducerContext = createContext()
export { ReducerContext }
const initState = reducers()

const Main = () => {
    const reducer = useReducer(reducers, initState)
    return (
        <ReducerContext.Provider value={reducer}>
            <Head />
            <Todo />
        </ReducerContext.Provider>
    )
}


ReactDom.render(<Main />, document.getElementById('root'))