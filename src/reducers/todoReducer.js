const initState = {
    list: [{ listKey: 1, name: 'first' }]
}

// 保留 Redux 的做法，直接將 initState 給 Reducer
const todoReducer = (state = initState, action) => {
    switch (action.type) {
        case 'ADD_TODO':
            let newKey = 1
            if (state.list.length !== 0){
                newKey = state.list[state.list.length - 1].listKey + 1
            }
            return {
                ...state,
                list: [...state.list, { listKey: newKey, name: action.payload.name }]
            }
        default:
            return state
    }
}

export { todoReducer }