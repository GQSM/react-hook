const initState = {
    todo: [{ listKey: 1, name: 'first' }]
}

const testReducer = (state = initState, action) => {
    switch (action.type) {
        case 'T_ADD_TODO':
            let newKey = 1
            if (state.todo.length !== 0){
                newKey = state.todo[state.todo.length - 1].listKey + 1
            }
            return {
                ...state,
                todo: [...state.todo, { listKey: newKey, name: action.payload.name }]
            }
        case 'T_REMOVE_TODO':
            let oldTodo = [...state.todo]
            let foundIndex = oldTodo.findIndex((list) => {
                return list.listKey === action.payload.listKey
            })
            oldTodo.splice(foundIndex, 1)
            let newTodo = [...oldTodo]
            return {
                ...state,
                todo: newTodo
            }
        default:
            return state
    }
}

export { testReducer }