import React, { useState, useContext } from 'react'
import { ReducerContext } from '../../index.jsx'

const Head = () => {
    const [state, dispatch] = useContext(ReducerContext)
    
    return (
        <div>
            {/* 單純列出筆數 */}
            <span>共有{state.todo.list.length}筆待辦事項</span>
        </div>
    )
}

export { Head }