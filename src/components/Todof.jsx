import React, { useState, useEffect } from 'react'

const Todo = props => {
    //設置 state
    const [listName, setListName] = useState('default value')

    //改變 state
    const changeListName = e =>{
        setListName(e.target.value) 
    }

    //生命週期
    useEffect(()=>{
        console.log(`只執行第一次`)
    },[listName])

    return (
        <input value={listName} onChange={changeListName} />
    )
}

export { Todo }

