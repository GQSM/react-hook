import React from 'react'

class Todo extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            listName: 'default value'
        }
        this.changeListName = this.changeListName.bind(this)
    }

    changeListName(e){
        this.setState({listName: e.target.value},
            ()=>{console.log(this.state)})
    }

    render(){
        return(
            <input name='listName' value={this.state.listName} 
                    onChange={this.changeListName} />
        )
    }
}

export {Todo}