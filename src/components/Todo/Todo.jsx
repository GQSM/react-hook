import React, { useState, useContext } from 'react'
import { ReducerContext } from '../../index.jsx'

const Todo = () => {
    const [listName, setListName] = useState('')
    const [state, dispatch] = useContext(ReducerContext)
    const addTodo = () => {
        dispatch({ type: 'ADD_TODO', payload: { name: listName } })
        setListName('')
    }
console.log(state)
    return (
        <div>
            <input value={listName}
                onChange={e => setListName(e.target.value)} />
            <input type='button' value='新增' onClick={addTodo} />
            {state.todo.list.map((list) => {
                return (
                    <p key={list.key}>
                        <input type='button' value='移除'
                            onClick={() => { removeTodo(list.listKey) }} />
                        {list.name}
                    </p>
                )
            })}
        </div>
    )
}

export { Todo }