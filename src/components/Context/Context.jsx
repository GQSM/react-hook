import React, { createContext, useContext } from 'react'

const ParentContext = createContext()

const Parent = () => {
    const title = 'Hi this is context!'
    return (
        <ParentContext.Provider value={title}>
            
        </ParentContext.Provider>
    )
}

const Son = () => {
    const title = useContext(ParentContext)
    return (
        <h1>{title}</h1>
    )
}